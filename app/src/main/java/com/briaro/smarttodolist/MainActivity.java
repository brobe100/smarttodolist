package com.briaro.smarttodolist;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private final String SAVED_TASKS_KEY = "SavedTasks";
    private ArrayAdapter<Task> mTaskListAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ArrayList<Task> list = getSavedTasks();

        final ArrayAdapter<Task> taskListAdaptor = new ArrayAdapter<>(this,
                R.layout.task_layout, list);
        mTaskListAdaptor = taskListAdaptor;


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptForNewTask(taskListAdaptor, view);
            }
        });

        ListView taskListView = (ListView) findViewById(R.id.taskListView);

        sortList(taskListAdaptor);

        taskListView.setAdapter(taskListAdaptor);

        taskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                promptForTaskComplete(taskListAdaptor, parent, view, position, list);
            }
        });

        taskListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                promptForTaskDelete(taskListAdaptor, parent, view, position, list);

                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTaskListAdaptor.notifyDataSetChanged();
    }

    private ArrayList<Task> getSavedTasks() {
        try {
            SharedPreferences pref = this.getPreferences(Context.MODE_PRIVATE);
            Set<String> currentTasksSet = pref.getStringSet(SAVED_TASKS_KEY, null);
            if (currentTasksSet == null)
                return new ArrayList<>();

            String[] currentTasksStringArray = currentTasksSet.toArray(new String[currentTasksSet.size()]);
            ArrayList<Task> currentTasks = new ArrayList<>();
            for (int i = 0; i < currentTasksSet.size(); ++i) {
                Task newTask = new Task();
                newTask.deserialize(currentTasksStringArray[i]);
                currentTasks.add(newTask);
            }

            return currentTasks;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private void promptForTaskDelete(final ArrayAdapter<Task> taskListAdaptor,
                                     final AdapterView<?> parent, final View view,
                                     final int position, final ArrayList<Task> list) {
        LayoutInflater li = LayoutInflater.from(this);

        View promptsView = li.inflate(R.layout.comfirm_delete, parent, false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                final Task item = (Task) parent.getItemAtPosition(position);
                                list.remove(item);

                                taskListAdaptor.notifyDataSetChanged();
                                view.setAlpha(1);

                                saveTasks(taskListAdaptor);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void promptForTaskComplete(final ArrayAdapter<Task> taskListAdaptor,
                                       final AdapterView<?> parent, final View view,
                                       final int position, final ArrayList<Task> list) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.comfirm_complete, parent, false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                taskClicked(parent, view, position, list, taskListAdaptor);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void promptForNewTask(final ArrayAdapter<Task> taskListAdaptor, View view) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.new_task, (ViewGroup) view.getParent(), false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptsView);

        final EditText editTextTaskName = (EditText) promptsView.
                findViewById(R.id.editTextTaskName);

        final EditText editTextDaysToRepeat = (EditText) promptsView.
                findViewById(R.id.editTextDaysToRepeat);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String taskName = editTextTaskName.getText().toString();
                                String daysToRepeatString = editTextDaysToRepeat.getText().toString();
                                int daysToRepeat = stringToInt(daysToRepeatString);
                                Task newTask = new Task(taskName, daysToRepeat);
                                addTaskToList(taskListAdaptor, newTask);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private int stringToInt(String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void addTaskToList(ArrayAdapter<Task> taskListAdaptor, Task newTask) {
        taskListAdaptor.add(newTask);
        sortList(taskListAdaptor);
        taskListAdaptor.notifyDataSetChanged();
        saveTasks(taskListAdaptor);
    }

    private void saveTasks(ArrayAdapter<Task> taskListAdaptor) {
        int taskCount = taskListAdaptor.getCount();
        Set<String> currentTasksSet = new HashSet<>();
        for (int i = 0; i < taskCount; ++i) {
            currentTasksSet.add(taskListAdaptor.getItem(i).serialize());
        }

        SharedPreferences pref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putStringSet(SAVED_TASKS_KEY, currentTasksSet);
        editor.apply();
    }

    private void sortList(ArrayAdapter<Task> taskListAdaptor) {
        taskListAdaptor.sort(new Comparator<Task>() {
            @Override
            public int compare(Task lhs, Task rhs) {
                return lhs.compareTo(rhs);   //or whatever your sorting algorithm
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void taskClicked(AdapterView<?> parent, final View view, int position, final ArrayList<Task> list, final ArrayAdapter<Task> taskListAdaptor) {
        final Task item = (Task) parent.getItemAtPosition(position);
        view.animate().setDuration(500).alpha(0)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        item.markCompleted();
                        if (item.isNotRepeated())
                            list.remove(item);

                        taskListAdaptor.notifyDataSetChanged();
                        view.setAlpha(1);
                        saveTasks(taskListAdaptor);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                showHelp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showHelp() {
        Intent intent = (new Intent(this, HelpActivity.class));
        startActivity(intent);
    }
}
