package com.briaro.smarttodolist;

import java.util.Calendar;
import java.util.Date;

class Task {
    private String description;
    private Date lastCompleted;
    private Integer daysToRepeat;
    private Boolean newTask = true;

    Task() {
        description = "";
        lastCompleted = Calendar.getInstance().getTime();
        daysToRepeat = 0;
    }

    Task(String description, int daysToRepeat) {
        this.description = description;
        this.lastCompleted = Calendar.getInstance().getTime();
        this.daysToRepeat = daysToRepeat;
    }

    private int getPriority() {
        Date today = Calendar.getInstance().getTime();
        long millisecondsSinceCompleted = today.getTime() - lastCompleted.getTime();

        long millisecondsToRepeat;
        if (daysToRepeat == 0)
            millisecondsToRepeat = (long) 24 * 60 * 60 * 1000;
        else
            millisecondsToRepeat = (long) daysToRepeat * 24 * 60 * 60 * 1000;

        int calculatedPriority = (int) (millisecondsSinceCompleted * 100 / millisecondsToRepeat);
        if (newTask)
            calculatedPriority += 100;

        return calculatedPriority;
    }

    public String toString() {
        double priority = (double) getPriority() / 100;
        String priorityString = String.format("%1$,.2f", priority);
        String repeatDaysString = "";
        if (daysToRepeat != 0) {
            repeatDaysString = " (every " + daysToRepeat.toString();
            if (daysToRepeat == 1)
                repeatDaysString += " day)";
            else
                repeatDaysString += " days)";
        }
        return priorityString + "  " + description + repeatDaysString;
    }

    public int compareTo(Task task) {
        return task.getPriority() - this.getPriority();
    }

    public String serialize() {
        String lastCompleteDateString = Long.toString(lastCompleted.getTime());
        String daysToRepeatString = Integer.toString(daysToRepeat);
        String newTaskString = Boolean.toString(newTask);
        return lastCompleteDateString + "," + daysToRepeatString + "," + newTaskString + "," +
                description;
    }

    public void deserialize(String serialString) {
        String[] splitSerialString = serialString.split(",", 4);
        String lastCompletedMillisecondsStrong = splitSerialString[0];
        long lastCompletedMilliseconds = Long.parseLong(lastCompletedMillisecondsStrong);
        lastCompleted = new Date(lastCompletedMilliseconds);

        daysToRepeat = Integer.parseInt(splitSerialString[1]);

        newTask = Boolean.parseBoolean(splitSerialString[2]);

        description = splitSerialString[3];
    }

    public boolean isNotRepeated() {
        return daysToRepeat == 0;
    }

    public void markCompleted() {
        newTask = false;
        lastCompleted = Calendar.getInstance().getTime();
    }
}
